pub mod expr;
pub mod lexer;
pub mod list;
pub mod token;

use token::collect;

use expr::{Expr, Value};
use list::List;

use crate::{lexer::Tokenizer, token::Token};

fn main() {
    expr();
    // expr();
    // let test = "(define x = 0 (x + 1))";
    // println!("{:#?}", vectorize(test));
}

fn tokens() {
    let test = "(define alma (x + y))";
    let tokenizer = Tokenizer::<Token>::new(test);
    println!("{:?}", tokenizer);
}

fn expr() {
    let test = "
    (const x true
        (const y (*x + *check)
            (:*bool y ? y + (10 / 2.5))))";
    let tokens = collect(Tokenizer::new(test));
    let null = Expr::Value(Value::None);
    let variables = List::new(("", &null));
    println!("{:#?}", Expr::new(variables, &tokens));
}
