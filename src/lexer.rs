use super::list::*;
use std::error::Error;
use std::fmt::Display;

pub trait Patterns<'a>: Sized {
    const PATTERNS: &'static [char];

    fn from_str(s: &'a str) -> Option<Self>;
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum ParseError {
    UnknownToken,
    Overflow,
}

impl Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Error for ParseError {}

#[derive(Copy, Clone, PartialEq)]
pub struct Tokenizer<'a, P: Patterns<'a>> {
    next: Option<P>,
    s: &'a str,
}

impl<'a, P: Patterns<'a>> Tokenizer<'a, P> {
    pub fn new(s: &'a str) -> Self {
        Self { s, next: None }
    }
}

impl<'a, P: Patterns<'a> + Clone + std::fmt::Debug> std::fmt::Debug for Tokenizer<'a, P> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_list().entries(&mut self.clone()).finish()
    }
}

impl<'a, P: Patterns<'a>> Iterator for Tokenizer<'a, P> {
    type Item = P;
    fn next(&mut self) -> Option<Self::Item> {
        self.next.take().or_else(|| {
            self.s.find(P::PATTERNS).and_then(|i| {
                if i > 0 {
                    let token = P::from_str(&self.s[..i].trim_start());
                    self.next = P::from_str(&self.s[i..i + 1]);
                    self.s = &self.s.get(i + 1..).unwrap_or(self.s);
                    token
                } else {
                    let token = P::from_str(&self.s[i..i + 1]);
                    self.s = &self.s.get(i + 1..).unwrap_or(self.s);
                    token
                }
            })
        })
    }
}

pub fn tokenize<'a, 's, F, P, U>(list: List<'a, U>, mut iter: impl Iterator<Item = P>, mut f: F)
where
    P: Patterns<'s>,
    F: for<'l> FnMut(&'l List<'l, U>, P) -> Option<List<'l, U>>,
{
    if let Some(t) = iter.next() {
        f(&list, t).map(|list| tokenize(list, iter, f));
    }
}
