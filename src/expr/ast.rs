use crate::{lexer::ParseError, list::List};

use super::Expression;
use crate::token::*;
pub use error::*;
use std::rc::Rc;

type Expressions<'a> = List<'a, (&'a str, &'a Expr)>;

const NONE: Value = Value::None;

mod error {
    use crate::token::RichToken;
    use std::error::Error;
    use std::fmt::Display;

    use super::{Operator, Value};

    #[derive(Clone, Debug, PartialEq)]
    pub struct BinOpErr {
        a: Value,
        b: Value,
        op: Operator,
    }

    impl Display for BinOpErr {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?} cannot \"{:?}\" {:?}", self.a, self.op, self.b)
        }
    }

    impl BinOpErr {
        pub fn new(op: Operator, a: Value, b: Value) -> Self {
            Self { op, a, b }
        }
    }

    #[derive(Clone, Debug, PartialEq)]
    pub struct InvOpErr {
        val: Value,
    }

    impl From<Value> for InvOpErr {
        fn from(val: Value) -> Self {
            Self { val }
        }
    }

    impl Display for InvOpErr {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "Illegal operation on {:?}", self.val)
        }
    }

    #[derive(Debug)]
    enum ErrorKind {
        InvalidToken,
        Overflow,
        Boxed(Box<dyn Error>),
    }

    #[derive(Debug)]
    pub struct ExprError<'a> {
        line: usize,
        e: ErrorKind,
        tokens: &'a [RichToken<'a>],
    }

    impl<'a> ExprError<'a> {
        pub fn overflow(line: usize, tokens: &'a [RichToken]) -> Self {
            Self {
                e: ErrorKind::Overflow,
                line,
                tokens,
            }
        }
        pub fn token(line: usize, tokens: &'a [RichToken]) -> Self {
            Self {
                e: ErrorKind::InvalidToken,
                line,
                tokens,
            }
        }
        pub fn boxed(e: Box<dyn Error>, line: usize, tokens: &'a [RichToken]) -> Self {
            Self {
                e: ErrorKind::Boxed(e),
                line,
                tokens,
            }
        }
    }

    impl Display for ExprError<'_> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{}: {:?}\n{:?}", self.line, self.e, self.tokens)
        }
    }

    impl Error for ExprError<'_> {}
    impl Error for BinOpErr {}
    impl Error for InvOpErr {}
}

#[derive(Clone, Debug, PartialEq)]
pub enum Value {
    None,
    Int(i32),
    Bool(bool),
    Float(f64),
    // String(String),
    /// A struct has a name and a list of values related to it.
    ///
    /// The layout of the struct can be infered by its name.
    ///
    /// Note: Maybe Value should take a T that is the sub type system of the struct.
    Struct(String, Vec<Value>),
}

impl Value {
    fn add(&self, other: &Self) -> Result<Self, BinOpErr> {
        match self {
            Self::None => return Ok(other.clone()),
            Self::Int(aint) => match other {
                Self::None => return Ok(self.clone()),
                Self::Float(f) => return Ok(Value::Float(*aint as f64 + f)),
                Self::Int(int) => return Ok(Value::Int(aint + int)),
                _ => {}
            },
            Self::Float(af) => match other {
                Self::None => return Ok(self.clone()),
                Self::Float(f) => return Ok(Value::Float(af + f)),
                Self::Int(int) => return Ok(Value::Float(af + *int as f64)),
                _ => {}
            },
            _ => {}
        }
        Err(BinOpErr::new(Operator::Add, self.clone(), other.clone()))
    }
    fn sub(&self, other: &Self) -> Result<Self, BinOpErr> {
        match self {
            Self::None => return Ok(other.clone()),
            Self::Int(aint) => match other {
                Self::None => return Ok(self.clone()),
                Self::Float(f) => return Ok(Value::Float(*aint as f64 - f)),
                Self::Int(int) => return Ok(Value::Int(aint - int)),
                _ => {}
            },
            Self::Float(af) => match other {
                Self::None => return Ok(self.clone()),
                Self::Float(f) => return Ok(Value::Float(af - f)),
                Self::Int(int) => return Ok(Value::Float(af - *int as f64)),
                _ => {}
            },
            _ => {}
        }
        Err(BinOpErr::new(
            Operator::Substract,
            self.clone(),
            other.clone(),
        ))
    }
    fn mul(&self, other: &Self) -> Result<Self, BinOpErr> {
        match self {
            Self::None => return Ok(other.clone()),
            Self::Int(aint) => match other {
                Self::None => return Ok(self.clone()),
                Self::Float(f) => return Ok(Value::Float(*aint as f64 * f)),
                Self::Int(int) => return Ok(Value::Int(aint * int)),
                _ => {}
            },
            Self::Float(af) => match other {
                Self::None => return Ok(self.clone()),
                Self::Float(f) => return Ok(Value::Float(af * f)),
                Self::Int(int) => return Ok(Value::Float(af * *int as f64)),
                _ => {}
            },
            _ => {}
        }
        Err(BinOpErr::new(
            Operator::Multiply,
            self.clone(),
            other.clone(),
        ))
    }
    fn div(&self, other: &Self) -> Result<Self, BinOpErr> {
        match self {
            Self::None => return Ok(other.clone()),
            Self::Int(aint) => match other {
                Self::None => return Ok(self.clone()),
                Self::Float(f) => return Ok(Value::Float(*aint as f64 / f)),
                Self::Int(int) => return Ok(Value::Int(aint / int)),
                _ => {}
            },
            Self::Float(af) => match other {
                Self::None => return Ok(self.clone()),
                Self::Float(f) => return Ok(Value::Float(af / f)),
                Self::Int(int) => return Ok(Value::Float(af / *int as f64)),
                _ => {}
            },
            _ => {}
        }
        Err(BinOpErr::new(
            Operator::Multiply,
            self.clone(),
            other.clone(),
        ))
    }
    fn and(&self, other: &Self) -> Result<Self, BinOpErr> {
        match self {
            Self::None => return Ok(other.clone()),
            Self::Bool(ab) => match other {
                Self::None => return Ok(self.clone()),
                Self::Bool(bb) => return Ok(Value::Bool(ab & bb)),
                _ => {}
            },
            _ => {}
        }
        Err(BinOpErr::new(Operator::And, self.clone(), other.clone()))
    }
    fn or(&self, other: &Self) -> Result<Self, BinOpErr> {
        match self {
            Self::None => return Ok(other.clone()),
            Self::Bool(ab) => match other {
                Self::None => return Ok(self.clone()),
                Self::Bool(bb) => return Ok(Value::Bool(ab | bb)),
                _ => {}
            },
            _ => {}
        }
        Err(BinOpErr::new(Operator::Or, self.clone(), other.clone()))
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Operator {
    // Transformation.
    Map,
    Add,
    Multiply,
    Divide,
    Substract,

    // Logic.
    And,
    Or,
    Equals,
    NotEquals,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum UnOperator {
    Not,
    Neg,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Expr {
    Value(Value),
    Var(String),
    Define(String, Rc<(Expr, Expr)>),
    IfElse(Rc<Expr>, Rc<(Expr, Expr)>),
    InvExpr(Rc<Expr>),
    BinExpr(Operator, Rc<(Expr, Expr)>),
}

impl From<Value> for Expr {
    fn from(val: Value) -> Self {
        Expr::Value(val)
    }
}

impl<'a> Expression<'a> for Expr {
    type Value = Value;

    // The ast is evaluated as it's being built.
    fn eval(&'a self, vars: super::Variables) -> Result<Value, Box<dyn std::error::Error>> {
        match self {
            Self::Value(val) => Ok(val.clone()),
            Self::Var(name) => {
                vars.iter()
                    .find(|(var, _)| name.eq(var))
                    .map(|(_, val)| (*val).clone())
                    // Create a better error.
                    .ok_or(Box::new(ParseError::UnknownToken))
            }
            Self::IfElse(expr, paths) => {
                let boolean = expr.eval(vars);
                let (a, b) = paths.as_ref();
                if let Ok(Value::None) = boolean {
                    return Ok(Value::None);
                } else if let Ok(Value::Bool(true)) = boolean {
                    return a.eval(vars);
                } else if let Ok(Value::Bool(false)) = boolean {
                    return b.eval(vars);
                }
                let a = a.eval(vars)?;
                let b = b.eval(vars)?;
                a.eq(&b)
                    .then(|| a)
                    // Create a better error.
                    .ok_or(Box::new(ParseError::UnknownToken))
            }
            Self::Define(name, expr) => {
                let val = expr.0.eval(vars)?;
                expr.1.eval(vars.push((name, &val)))
            }
            Self::BinExpr(op, expr) => {
                let (a, b) = expr.as_ref();
                let a = a.eval(vars)?;
                let b = b.eval(vars)?;
                let r = match op {
                    Operator::Add => a.add(&b),
                    Operator::Substract => a.sub(&b),
                    Operator::Multiply => a.mul(&b),
                    Operator::Divide => a.div(&b),
                    Operator::And => a.and(&b),
                    Operator::Or => a.or(&b),
                    Operator::Equals => Ok(Value::Bool(a.eq(&b))),
                    _ => todo!(),
                };
                match r {
                    Ok(s) => Ok(s),
                    Err(e) => Err(Box::new(e)),
                }
            }
            Self::InvExpr(expr) => expr.eval(vars).and_then(|val| match val {
                Value::Bool(b) => Ok(Value::Bool(!b)),
                Value::Int(int) => Ok(Value::Int(-int)),
                _ => Err(Box::new(InvOpErr::from(val))),
            }),
        }
    }
}

fn clamp<'a>(tokens: &'a [RichToken], token: Token) -> Option<(&'a [RichToken<'a>], usize)> {
    tokens
        .iter()
        .position(|t| t.token == token)
        .and_then(|l| tokens[l].complement.map(|r| (l, r)))
        .map(|(l, r)| (&tokens[l + 1..l + r], r + 1))
}

fn next<'a>(tokens: &'a [RichToken], token: Token) -> Option<(&'a [RichToken<'a>], usize)> {
    if tokens[0].token == token {
        clamp(tokens, token)
    } else {
        match tokens[0].token {
            Token::Asterisk | Token::Exclamation => tokens.get(0..2).map(|t| (t, 2)),
            _ => tokens.get(0..1).map(|t| (t, 1))
        }
    }
}

fn parse_op<'a>(
    mut iter: impl Iterator<Item = Token<'a>>,
    line: usize,
    tokens: &'a [RichToken],
) -> Result<(usize, Operator), ExprError<'a>> {
    let mut i = 1;
    let op = iter
        .next()
        .ok_or(ExprError::overflow(line, tokens))
        .and_then(|t| match t {
            Token::Arrow => Ok(Operator::Map),
            Token::Vbar => Ok(Operator::Or),
            Token::And => Ok(Operator::And),
            Token::Cross => Ok(Operator::Add),
            Token::Hyphen => Ok(Operator::Substract),
            Token::Slash => Ok(Operator::Divide),
            Token::Asterisk => Ok(Operator::Multiply),
            Token::Equal => Ok(Operator::Equals),
            Token::Exclamation => iter
                .next()
                .filter(|t| Token::Equal.eq(t))
                .map(|_| {
                    i += 1;
                    Operator::NotEquals
                })
                .ok_or(ExprError::token(line, tokens)),
            _ => Err(ExprError::token(line, tokens)),
        })?;
    Ok((i, op))
}

impl Expr {
    pub fn new<'t>(
        macros: Expressions,
        mut tokens: &'t [RichToken],
    ) -> Result<Self, ExprError<'t>> {
        let vars = List::new(("", &NONE));
        if let Some(Token::LeftParen) = tokens.get(0).map(|t| t.token) {
            let line = tokens[0].line;
            let (l_tokens, i) =
                next(tokens, Token::LeftParen).ok_or(ExprError::overflow(line, tokens))?;
            if let Some(tokens) = tokens.get(i..).filter(|t| !t.is_empty()) {
                let iter = tokens.iter();
                let l_expr = Expr::new(macros, l_tokens)?;
                return parse_op(iter.map(|t| t.token), line, tokens)
                    .and_then(|(i, op)| {
                        Expr::new(
                            macros,
                            &tokens.get(i..).ok_or(ExprError::overflow(line, tokens))?,
                        )
                        .map(|expr| Self::BinExpr(op, Rc::new((l_expr.clone(), expr))))
                    })
                    .map(|e| e.eval(vars).map(Expr::from).unwrap_or_else(|_| e));
            } else {
                return Expr::new(macros, l_tokens)
                    .map(|e| e.eval(vars).map(Expr::from).unwrap_or_else(|_| e));
            }
        } else if tokens.len() > 0 {
            let line = tokens[0].line;
            let mut iter = tokens.iter();
            match iter.next().unwrap().token {
                // Compile time variables.
                Token::Keyword(Keyword::Const) => {
                    let name = iter
                        .next()
                        .ok_or(ExprError::overflow(line, tokens))?
                        .token
                        .literal()
                        .unwrap();
                    tokens = tokens.get(2..).ok_or(ExprError::overflow(line, tokens))?;
                    let (l_tokens, i) =
                        next(tokens, Token::LeftParen).ok_or(ExprError::overflow(line, tokens))?;
                    let r_tokens = tokens.get(i..).ok_or(ExprError::overflow(line, tokens))?;
                    let def = Expr::new(macros, l_tokens)?;
                    let def = def.eval(vars).map(Expr::from).unwrap_or_else(|_| def);
                    return Expr::new(macros.push((name, &def)), r_tokens);
                }
                // Run time variables
                Token::Keyword(Keyword::Define) => {
                    let name = iter
                        .next()
                        .ok_or(ExprError::overflow(line, tokens))?
                        .token
                        .literal()
                        .unwrap();
                    tokens = tokens.get(2..).ok_or(ExprError::overflow(line, tokens))?;
                    let (l_tokens, i) =
                        next(tokens, Token::LeftParen).ok_or(ExprError::overflow(line, tokens))?;
                    let r_tokens = tokens.get(i..).ok_or(ExprError::overflow(line, tokens))?;
                    let def = Expr::new(macros, l_tokens)?;
                    let def = def.eval(vars).map(Expr::from).unwrap_or_else(|_| def);
                    let expr = Expr::new(macros, r_tokens)?;
                    return Ok(Expr::Define(name.to_owned(), Rc::new((def, expr))));
                }
                Token::TwoPoint => {
                    tokens = &tokens.get(1..).ok_or(ExprError::overflow(line, tokens))?;
                    let (e_tokens, i) =
                        next(tokens, Token::LeftParen).ok_or(ExprError::overflow(line, tokens))?;
                    tokens = &tokens.get(i..).ok_or(ExprError::overflow(line, tokens))?;
                    let (t_tokens, i) =
                        next(tokens, Token::LeftParen).ok_or(ExprError::overflow(line, tokens))?;
                    tokens = &tokens.get(i..).ok_or(ExprError::overflow(line, tokens))?;

                    let expr = Expr::new(macros, e_tokens)?;
                    let t_expr = Expr::new(macros, t_tokens)?;
                    let expr = expr.eval(vars).map(Expr::from).unwrap_or_else(|_| expr);
                    let t_expr = t_expr.eval(vars).map(Expr::from).unwrap_or_else(|_| t_expr);

                    return tokens
                        .iter()
                        .next()
                        .filter(|t| t.token == Token::Question)
                        .ok_or(ExprError::overflow(line, tokens))
                        .and_then(|_| {
                            let f_tokens = 
                                tokens.get(1..).ok_or(ExprError::overflow(line, tokens))?;
                            Expr::new(macros, f_tokens).map(|f_expr| {
                                Expr::IfElse(
                                    Rc::new(expr.clone()),
                                    Rc::new((t_expr.clone(), f_expr)),
                                )
                            })
                        })
                        .map(|e| e.eval(vars).map(Expr::from).unwrap_or_else(|_| e));
                }
                Token::Exclamation | Token::Hyphen => {
                    tokens = tokens.get(1..).ok_or(ExprError::overflow(line, tokens))?;
                    let (l_tokens, i) =
                        next(tokens, Token::LeftParen).ok_or(ExprError::overflow(line, tokens))?;
                    tokens = &tokens.get(i..).ok_or(ExprError::overflow(line, tokens))?;
                    iter = tokens.iter();

                    if tokens.len() > 1 {
                        let i_expr = Expr::InvExpr(Rc::new(Expr::new(macros, l_tokens)?));
                        return parse_op(iter.map(|t| t.token), line, tokens)
                            .and_then(|(i, op)| {
                                Expr::new(
                                    macros,
                                    &tokens.get(i..).ok_or(ExprError::overflow(line, tokens))?,
                                )
                                .map(|expr| Self::BinExpr(op, Rc::new((i_expr, expr))))
                            })
                            .map(|e| e.eval(vars).map(Expr::from).unwrap_or_else(|_| e));
                    } else {
                        return Ok(Expr::InvExpr(Rc::new(Expr::new(macros, l_tokens)?)));
                    }
                }
                Token::Asterisk => {
                    let name = iter
                        .next()
                        .ok_or(ExprError::overflow(line, tokens))?
                        .token
                        .literal()
                        .unwrap();
                    let var = Expr::Var(name.to_owned());
                    if tokens.len() > 2 {
                        tokens = &tokens[2..];
                        return parse_op(iter.map(|t| t.token), line, tokens)
                            .and_then(|(i, op)| {
                                Expr::new(
                                    macros,
                                    &tokens
                                        .get(i..)
                                        .ok_or(ExprError::overflow(line, tokens))?,
                                )
                                .map(|expr| Self::BinExpr(op, Rc::new((var.clone(), expr))))
                            })
                            .map(|e| e.eval(vars).map(Expr::from).unwrap_or_else(|_| e));
                    } else {
                        return Ok(var);
                    }
                }
                Token::Literal(val) => {
                    let l_expr = val
                        .parse::<i32>()
                        .map(|int| Expr::Value(Value::Int(int)))
                        .ok()
                        .or_else(|| {
                            val.parse::<bool>()
                                .map(|b| Expr::Value(Value::Bool(b)))
                                .ok()
                        })
                        .or_else(|| {
                            val.parse::<f64>()
                                .map(|f| Expr::Value(Value::Float(f)))
                                .ok()
                        })
                        .or_else(|| val.eq("null").then(|| Expr::Value(Value::None)))
                        .or_else(|| {
                            macros
                                .iter()
                                .find(|(v, _)| &val == v)
                                .map(|(_, expr)| (*expr).clone())
                        })
                        .ok_or(ExprError::token(line, tokens))?;
                    if tokens.len() > 1 {
                        return parse_op(iter.map(|t| t.token), line, tokens)
                            .and_then(|(i, op)| {
                                Expr::new(
                                    macros,
                                    &tokens
                                        .get(i + 1..)
                                        .ok_or(ExprError::overflow(line, tokens))?,
                                )
                                .map(|r_expr| Self::BinExpr(op, Rc::new((l_expr.clone(), r_expr))))
                            })
                            .map(|e| e.eval(vars).map(Expr::from).unwrap_or_else(|_| e));
                    } else {
                        return Ok(l_expr);
                    }
                }
                _ => {
                    return Err(ExprError::token(line, tokens));
                }
            }
        }
        Err(ExprError::token(
            tokens.get(0).map(|t| t.line).unwrap_or_default(),
            tokens,
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::Value;

    #[test]
    fn values() {
        assert_eq!(Ok(Value::Int(10)), Value::Int(2).mul(&Value::Int(5)));
        assert_eq!(Ok(Value::Int(7)), Value::Int(2).add(&Value::Int(5)));
        assert_eq!(Ok(Value::Int(-3)), Value::Int(2).sub(&Value::Int(5)));
        assert_eq!(Ok(Value::Int(2)), Value::Int(10).div(&Value::Int(5)));
        assert_eq!(
            Ok(Value::Bool(false)),
            Value::Bool(true).and(&Value::Bool(false))
        );
    }
}
