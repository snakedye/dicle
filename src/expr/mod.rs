mod ast;

use super::list::List;
pub use ast::*;
use std::error::Error;

type Variables<'a> = List<'a, (&'a str, &'a Value)>;

pub trait Expression<'a> {
    type Value: 'a;

    fn eval(&'a self, vars: Variables) -> Result<Self::Value, Box<dyn Error>>;
}
