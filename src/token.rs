use super::lexer::{tokenize, Patterns};
use super::list::List;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Keyword {
    Define,
    Const,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Token<'a> {
    LeftParen,
    RightParen,
    LeftBracket,
    RightBracket,
    Hyphen,
    Colon,
    Semicolon,
    TwoPoint,
    Vbar,
    Arrow,
    Point,
    Cross,
    Asterisk,
    Exclamation,
    Question,
    Empty,
    Equal,
    Slash,
    Backslash,
    NotEqual,
    Whitespace,
    Return,
    Tab,
    And,
    Keyword(Keyword),
    Literal(&'a str),
}

impl<'a> Token<'a> {
    pub fn literal(&'a self) -> Option<&'a str> {
        if let Token::Literal(s) = self {
            Some(s)
        } else {
            None
        }
    }
    pub fn is_whitespace(&self) -> bool {
        Token::Whitespace.eq(self)
    }
}

impl<'a> Patterns<'a> for Token<'a> {
    const PATTERNS: &'static [char] = &['(', ')', '[', ']', ':', ',', ';', ' ', '*', '!', '\n'];

    fn from_str(s: &'a str) -> Option<Self> {
        match s {
            "(" => Some(Self::LeftParen),
            ")" => Some(Self::RightParen),
            "[" => Some(Self::LeftBracket),
            "]" => Some(Self::RightBracket),
            "," => Some(Self::Colon),
            ";" => Some(Self::Semicolon),
            ":" => Some(Self::TwoPoint),
            "-" => Some(Self::Hyphen),
            "+" => Some(Self::Cross),
            "*" => Some(Self::Asterisk),
            "|" | "||" => Some(Self::Vbar),
            "=>" => Some(Self::Arrow),
            "==" | "=" => Some(Self::Equal),
            "!=" => Some(Self::NotEqual),
            "\n" => Some(Self::Return),
            "\t" => Some(Self::Tab),
            "." => Some(Self::Point),
            "!" => Some(Self::Exclamation),
            "?" => Some(Self::Question),
            "&" | "&&" => Some(Self::And),
            " " => Some(Self::Whitespace),
            "\\" => Some(Self::Backslash),
            "/" => Some(Self::Slash),
            "def" | "define" => Some(Self::Keyword(Keyword::Define)),
            "const" => Some(Self::Keyword(Keyword::Const)),
            _ => Some(Self::Literal(s)),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct RichToken<'a> {
    pub token: Token<'a>,
    pub line: usize,
    pub complement: Option<usize>,
}

impl<'a> From<Token<'a>> for RichToken<'a> {
    fn from(token: Token<'a>) -> Self {
        RichToken {
            token,
            line: 0,
            complement: None,
        }
    }
}

impl<'a> RichToken<'a> {
    fn new(token: Token<'a>, line: usize, index: usize) -> Self {
        Self {
            token,
            line,
            complement: Some(index),
        }
    }
    fn line(mut self, line: usize) -> Self {
        self.line = line;
        self
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Scope {
    Paren,
    Bracket,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Hint {
    Value,
    BinExp,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Metadata {
    scope: Scope,
    index: usize,
}

impl Metadata {
    fn scope(&self, scope: Scope) -> Self {
        let mut meta = *self;
        meta.scope = scope;
        meta
    }
    fn index(&self, index: usize) -> Self {
        let mut meta = *self;
        meta.index = index;
        meta
    }
}

impl Default for Metadata {
    fn default() -> Self {
        Self {
            scope: Scope::Paren,
            index: 0,
        }
    }
}

pub fn collect<'a>(tokens: impl Iterator<Item = Token<'a>>) -> Vec<RichToken<'a>> {
    let mut i: i32 = 0;
    let mut lines = 0;
    let mut vec: Vec<RichToken> = Vec::new();

    tokenize(
        List::new(Metadata::default()),
        tokens.filter_map(|t: Token| (!t.is_whitespace()).then(|| t)),
        |list, t| {
            let l_list;
            match t {
                Token::LeftParen => {
                    vec.push(RichToken::from(t).line(lines));
                    l_list = list.push(list.scope(Scope::Paren).index(i.max(0) as usize));
                }
                Token::LeftBracket => {
                    vec.push(RichToken::from(t).line(lines));
                    l_list = list.push(list.scope(Scope::Bracket).index(i.max(0) as usize));
                }
                Token::RightParen => {
                    vec[list.index].complement = Some(i.max(0) as usize - list.index);
                    vec.push(RichToken::new(t, lines, list.index));
                    l_list = (list.scope == Scope::Paren)
                        .then(|| list.previous().cloned())
                        .flatten()?;
                }
                Token::RightBracket => {
                    vec[list.index].complement = Some(i.max(0) as usize - list.index);
                    vec.push(RichToken::new(t, lines, list.index));
                    l_list = (list.scope == Scope::Bracket)
                        .then(|| list.previous().cloned())
                        .flatten()?;
                }
                Token::Return => {
                    i -= 1;
                    lines += 1;
                    l_list = *list;
                }
                _ => {
                    l_list = *list;
                    vec.push(RichToken::from(t).line(lines))
                }
            }
            i += 1;
            Some(l_list)
        },
    );

    vec
}

#[cfg(test)]
mod tests {
    use crate::lexer::Tokenizer;

    use super::Keyword;
    use super::Token;

    #[test]
    fn iter() {
        let test = "(define alma (x + y))";
        let tokenizer = Tokenizer::<Token>::new(test);
        assert_eq!(
            true,
            [
                Token::LeftParen,
                Token::Keyword(Keyword::Define),
                Token::Literal("alma"),
                Token::LeftParen,
                Token::Literal("x"),
                Token::Cross,
                Token::Literal("y"),
                Token::RightParen,
                Token::RightParen
            ]
            .into_iter()
            .eq(tokenizer.filter_map(|t| (!t.is_whitespace()).then(|| t)))
        )
    }
}
